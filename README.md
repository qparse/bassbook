# FiloBass: preliminary study
first case study for the preparation of FiloBass dataset (BassBook) with qparse.
transcriptions of bass lines.

It's all the same piece/recording:

> Have You Met Miss Jones
  Steve Gilmore (bass)

there are two sources for the transcription:

- one being the published score,  
  `book_transcription/` 
- the other being my manual annotations from Melodyne.  
  `manual_transcription/`

Hopefully the filenames are self explanatory.

The results of parsing with qparse are in `parsing/`,  
see `parsing/README.md` for an analysis of the results.


## Have You Met Miss Jones 
standard (1937)
written by Richard Rodgers for the musical comedy "I'd Rather Be Right".

some recordings:
- https://www.jazzstandards.com/compositions-2/haveyoumetmissjones.htm
- https://www.radiofrance.fr/francemusique/podcasts/repassez-moi-l-standard/repassez-moi-l-standard-have-you-met-miss-jones-written-by-richard-rodgers-lyricist-lorenz-hart-1937-7740000







