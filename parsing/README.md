# Experiments with "Have You Met Miss Jones"
https://gitlab.inria.fr/qparse/bassbook

## Compiling

### Make a command line target `monoparse`
#### Using CMake

In your favorite terminal:

```
git clone https://gitlab.inria.fr/qparse/qparselib.git
cd qparselib
git branch pybind
mkdir build; cd build
cmake ..
make monoparse
```

If everything goes well, you should get a target `monoparse` in the build directory.

You will use this target in this repo by using this path to monoparse.

#### Using XCode

TODO

#### compile options

to try these command lines with another target,
in `monoparse.cpp`, line 333:

```c
parsing::Model pmodel = parsing::Model::MonoLR; 
```

replace `parsing::Model::MonoLR` by `parsing::Model::MonoAlign`
it is an older model for parsing, better tested


### Make a python binding 

same first terminal commands as above, then

```
make pyqparse
```



## Examples with command line exec
The following command lines should be executed from the toplevel directory `bassbook`.

In order to produce an MEI output, `-out missjones.mei`. When no `-out` argument is given, you can see the score model printed in the terminal.

### A few command line options

First, here is an overview of some of the qparse options:

- `-v` : verbosity level (0..6)

- `-a` : a `.wta` file describing a tree grammar  
  in this experiment, the minimalistic grammar file `parsing/44-mini.wta` makes the job

- `-m` : path to midi input file  
  in this experiment: `manual_transcription/melodyne_transcribed_midi.mid`

- `start` : start date, in seconds, (for parsing) in input midi file
- `end` : end date, in seconds, (for parsing) in input midi file

- `barsec` : fixed measure duration in seconds

- `tempo` : fixed (expected) tempo in bpm

- `beats` : name of file containing beat-tracking information, in order to track the boundaries of measures

- `-config` : a `.ini` file with parameters

- `-mono` : treat the input midi file as monophonic (no argument)

- `-ts` : time signature

- `-key` : key signature

- `-clef` : clef specified

- `-max` : compute the best parse tree (no argument)
- `-min` : compute the worst parse tree 

- `-out` : name of out file

### Example with a beat file
This option gives the best results.  
When parsing with the option `-beats`, the measures boudaries are defined according to the timestep in the `-beats` file.  
The format of this file is described in the next paragraph.

```
/path/to/.../monoparse -v 5 -a parsing/44-red.wta -m manual_transcription/melodyne_transcribed_midi.mid -beats parsing/melodyne_transcribed_midi_bars.txt -start 0.99 -end 376 -config parsing/params.ini -mono -ts 4/4 -max
```

the output can be found in files:

- `parsing/missjones.mei`
- `parsing/missjones.pdf` (print with Verovio viewer, see below)

The results are lots better that with `-barsec` (no lag).

~~There is a bug in the display of dots in the last bar (it will be fixed).~~ fixed.

For a presentation without dots, see  

- `parsing/missjones-tie.mei`
- `parsing/missjones-tie.pdf`

### Example with option `-barsec`

The measures are computed according to the value given with `-barsec`, i.e. the real-time duration of 1 bar specified (default is 1) .

```
/path/to/.../monoparse 
-v 5 
-a parsing/44-simple.wta 
-m manual_transcription/melodyne_transcribed_midi.mid 
-config parsing/params.ini 
-mono 
-barsec 1.12 
-start 0.99 -end 40  
-ts 4/4 
-clef F4 
-key -1 
-max 
```

some results of the experiment with `-barsec 1.12` are commented in:
```
missjones-barsec112-anote.pdf
```
there are many lags.

### Example with a given tempo

to give start end dates (clip input), add `-start 0.99 -end 40` (0.99 and 40 are timestamps from the audio).

Here, the measures are computed according to the tempo, provided by the option `-tempo`.

```
/path/to/.../monoparse  
-v 5 
-a parsing/44-simple.wta 
-m manual_transcription/melodyne_transcribed_midi.mid 
-config parsing/params.ini 
-mono 
-tempo 220 
-start 0.99 -end 40  
-ts 4/4 
-clef F4 
-key -1 
-max 
```

### Making a beat-tracking files
the `-beats` file for the Miss Jones example is
`parsing/melodyne_transcribed_midi_bars.txt`

The main part of this `.txt` file file is a sequence of "ticks" = timestamps, one tick per line.

The meaning of the lines in the preamble is the following:  

- `file:`: midi file corresponding to this beattracking file
- `tpb:`: number of ticks per beat
- `bpb:`: number of beats per bar (downbeat)
- `up:`: number of ticks in the pickup bar (anacrousis), i.e. bebore the first downbeat.
- `tempo:`: tempo in the midi file. optimnal.
- `dur:`: duration (seconds) of the midi file. optional.

in the example, `tpb = 1` and `bpb = 1`, hence there is 1 tick per bar, and `up = 0` (no pickup bar).


The list of timestamps following the preamble was obtained from the dates of the *tempo change* in the parsed midi file
`manual_transcription/melodyne_transcribed_midi.mid`.  

- `parsing/melodyne_transcribed_midi_tempochange.txt`: timestamps of the tempo changes in `manual_transcription/melodyne_transcribed_midi.mid`
  extracted with `scripts/utils.py`, based on lib [pretty_midi](http://craffel.github.io/pretty-midi/) by Colin Raffel.
- `parsing/shifted.txt` same content shifted by 0.99s (as in option -start 0.99), for inspection, with bar durations.
- `parsing/melodyne_transcribed_midi_tempochange30.txt` : timestamps of the tempo changes for the first 30 measures.

`shifted.txt` show bar duration of about 970ms to 1170ms, and is also shows that some bars were skip.
We estimated the missing bar start dates by interpolation with `scripts/barinterpol.py`.


we have also made of beattracking on audio files with the plugin [beatroot](https://code.soundsoftware.ac.uk/projects/beatroot) for Sonic Visualizer, see  

- `separated_audio_beatroot.txt`
- `separated_audio_beatroot30.txt`
- ~~`melodyne_transcribed_midi.txt` : timestamps computed from audio file~~


### Example of parsing the 2 first measures to an MEI file

```
/path/to/.../monoparse 
-v 5 
-a parsing/44-mini.wta 
-m manual_transcription/melodyne_transcribed_midi.mid 
-config parsing/params.ini 
-mono 
-barsec 1.12 
-start 0.99 -end 3.28 
-ts 4/4 
-key -1 
-clef F4 
-max 
-out missjones.mei
```

### Input

`melodyne_transcribed_midi.log` contains a dump of the content of MIDI input, as read by qparse


### Parameters file

In file `param.ini`.

- coeff alpha:  
  mult. coeff for distance, for product with weight of tree, ie:  
  ```weight = weight_tree + (alpha * distance)```

### Schema file

`.wta` file

- `44-complete.wta` : complete model for 4/4 measure

- `44-red.wta`  : reduced model for 4/4 measure

- `44-mini.wta` : even more reduced model for 4/4 measure

- `44-simple.wta` : simplified model for 4/4 measure


## Examples with Python binding

see `scripts/script.py`

TODO




## MEI visualization

### Verovio viewer

<https://www.verovio.org/mei-viewer.xhtml>

### Francesco's Plugin for VSCode

### meise Editor
<http://meise.de.dariah.eu>

### sample encodings
<https://github.com/music-encoding/sample-encodings>


______________
DRAFT & TODO notes


**formula for distance computation**  
`src/weight/Distance.cpp`,  
function `distance(isegment, jam, date)`  & callee `distanceNote` etc.

## coefficient of swing

not implemented

division in 2 parts of an interval (for a duplet): 

- was: $\frac{1}{2}$ et $\frac{1}{2}$ for the resp. durations of sub-intervals
- todo: $\frac{1}{2} C_{swing}$  et $\frac{1}{2} (1-C_{swing})$ 

specification of swing in the tree grammar file:

- duplet symbole with *swing*  
  `T2s` (beamed) or `U2s` (unbeamed)
- swing coeff. in preamble ?
