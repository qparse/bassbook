import os
from pathlib import Path
from tkinter import N
import numpy

file_name = "melodyne_transcribed_midi_tempochange.txt"

dates = numpy.loadtxt(file_name, dtype="float")

# shift all dates in the array a by duration d
def shift(a, d):
    for i in range(0, len(a)-1):
        a[i] = a[i] - d

for b in range(0, len(dates)):
    print("{:3}: {:3.2f}".format(b, dates[b]))

# print the content of the array of dates a
def dump(a):
    for b in range(0, len(a)-1):
        print("{:3}: {:3.2f} ({:.2f})".format(b, a[b], a[b+1]-a[b]))


def interpolate(a, p = False):
    date = a[0]
    pdur = a[1] - a[0]
    sum = pdur
    mean = pdur
    nbar=1         # bar nb
    for b in range(0, len(a)-1):
        assert(date == a[b])    
        dur = a[b+1] - date
        # some bars skipped: we split
        if (dur > (1.75 * pdur)):    # TODO use 175*mean dur instead of 1/75*pdur ?
            n = round(dur/pdur) # number of bars
            dur = dur / n       # duration of split
            assert(n > 1)
            for c in range(0, n-1): # all splits but last
                sum = sum+dur
                mean = sum / nbar   
                if p:
                    print('{} ({}+{}): {:.2f}  ({:.2f}) mean={}'.format(nbar, b, c, date, dur, mean))
                else:
                    print(date)
                nbar = nbar + 1
                date = date + dur
        # last split (to next boundary) or normal case
        pdur = a[b+1] - date
        sum = sum+pdur
        mean = sum / nbar
        if p:
            print('{} ({}  ): {:.2f}  ({:.2f}) mean={}'.format(nbar, b, date, pdur, mean))
        else:
            print(date)
        nbar = nbar + 1
        date = a[b+1]        
    assert(date == a[len(a)-1]) # final date  
    print(date)

