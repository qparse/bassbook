import sys
import logging
from pyqparse import *


env = ScoringEnv()

set_config(0.5)

env.set_debug(True)

# set bar duration (?)
env.set_tempo(1.12, Tempo.Constant) # tempo model = constant tempo
assert env.has_tempo()
env.set_time_sig(4, 4)

# import MIDI input
env.set_open(True) # input time interval is right open
file_name_midi = "../manual_transcription/melodyne_transcribed_midi.mid"
format = Format.MIDI
mode = Mode.Mono
status = env.import_file(file_name_midi, format, mode)
assert status == 0
assert env.has_segment()
assert not env.empty_input()
assert env.size_input() > 0
assert env.duration_input() > 0

# load the grammar 
wta_filepath = "./44-mini.wta"
env.set_weight(WeightCode.Tropical) # redundant with arg. import_WTA ?
env.import_WTA(wta_filepath, WeightCode.Tropical)


# trivial voicing for mono mode (1 voice)
status = env.create_voicing(mode)
assert status == 0
env.revoice()

# parsing and score model construction
env.set_order(True) # enumeration ordering: from best to worst
status = env.open_score("score") # open new score (empty)
assert(status)
env.add_part("part1", Model.MonoLR, 1) # 1-best


print_score_model(env)

