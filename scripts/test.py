"""
Test :
python3 test.py 
"""

import sys
import logging

sys.path.insert(0, "../../build")
from pyqparse import *

# wta_filepath = "../schema/drum-44.wta"
# file_name_midi = "../drum/drum_tests_martin/2_tests/2_midi_pour_les_tests/midi_test.mid"
# file_name_serial = "../serial/input-02.seg"
# file_name_dot = "../dot/input-00.txt"
# file_name_json = "../json/test2.json"

wta_filepath = "./44-mini.wta"
file_name_midi = "../manual_transcription/melodyne_transcribed_midi.mid"


def test_empty_segment():
    env = InputEnv()
    print("Has segment must be false: ", env.has_segment())
    print("Empty Input must be true: ", env.empty_input())


def import_file(env, mode, file_format, file_name="", verbose=False):
    assert isinstance(file_format, str)
    assert isinstance(mode, Mode)

    if file_format == 'midi':
        format = Format.MIDI
        if not file_name: file_name = file_name_midi
    elif file_format == 'serial':
        format = Format.Serial
        if not file_name: file_name = file_name_serial
    elif file_format == 'dot':
        format = Format.Dot
        if not file_name: file_name = file_name_dot
    elif file_format == 'json':
        format = Format.JSON
        if not file_name: file_name = file_name_json
    else:
        logging.warning("import_file : {file_format} doesn't allow importing segments.")
        format = Format.Unknown
        file_name = ""

    assert file_name

    status = env.import_file(file_name, format, mode)
    assert not status, f"status is {status}, should be 0"

    env.create_voicing(mode)

    assert env.has_heap()
    assert env.has_segment()
    assert not env.empty_input()
    assert env.size_input() > 0
    assert env.duration_input() > 0


    if verbose:
        last = env.size_input()-1

        print("Has segment must be true: ",  env.has_segment())
        print("Empty Input must be false: ", env.empty_input())
        print("Size Input: ",                env.size_input())
        print("Duration Input: ",            env.duration_input())
        print("First Input Date:",           env.get_input_date(0))
        print("First Input MIDI Key:",       env.get_input_MIDI_key(0))
        print("First Input Onset:",          env.get_input_onset(0))
        print("Last Input Date:",            env.get_input_date(last))
        print("Last Input MIDI Key:",        env.get_input_MIDI_key(last))
        print("Last Input Offset:",          env.get_input_offset(last))

    return file_name


def incremental_input(env, mode, verbose=False):
    env.create_input(mode, 0, 0.024)

    env.add_input(0,       True,  44, 120)
    env.add_input(0.00456, False, 44, 0)
    env.add_input(0.00839, True,  52, 98)
    env.add_input(0.01267, False, 52, 0)
    env.add_input(0.01569, True,  42, 63)
    env.add_input(0.024,   False, 42, 0)

    assert env.has_segment()
    assert not env.empty_input()
    assert env.size_input() > 0
    assert env.duration_input() > 0

    if verbose:
        last = env.size_input()-1

        print("Has segment must be true: ",  env.has_segment())
        print("Empty Input must be false: ", env.empty_input())
        print("Size Input: ",                env.size_input())
        print("Duration Input: ",            env.duration_input())
        print("First Input Date:",           env.get_input_date(0))
        print("First Input MIDI Key:",       env.get_input_MIDI_key(0))
        print("First Input Onset:",          env.get_input_onset(0))
        print("Last Input Date:",            env.get_input_date(last))
        print("Last Input MIDI Key:",        env.get_input_MIDI_key(last))
        print("Last Input Offset:",          env.get_input_offset(last))


def test_scoring():
    env = ScoringEnv()

    set_config(0.5)

    env.set_debug(True)
    env.set_open(True)
    env.set_order(True)
    env.set_weight(WeightCode.Tropical)
    env.set_tempo(1.12, Tempo.Constant)
    assert env.has_tempo()

    env.set_time_sig(4, 4)

    file_name = import_file(env, Mode.Mono, 'midi')
    env.import_WTA(wta_filepath, WeightCode.Tropical)
    assert env.has_pool()

    assert env.has_segment()

    # print_segment(env)

    status = env.open_score(file_name)
    assert(status)

    env.add_part("part1", Model.MonoLR, 1)

    # print_trees(env, Model.MonoLR, 1)
    print_score_model(env)


def main():
    test_scoring()


    


if __name__ == "__main__":
    main()
