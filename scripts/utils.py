import os
from pathlib import Path
import pretty_midi as pm
import sys

# file_path (str) : path to the midi file
# parse_path (str) : path where you want the result file to be. 
# If None, it'll be in the current dir.
def write_tempo_changes(file_path, parse_path=""):
	midi_data = pm.PrettyMIDI(file_path)
	file_name = Path(file_path).stem

	tempo_changes_times, tempi = midi_data.get_tempo_changes()

	with open(os.path.join(parse_path, file_name + '_beats.txt'), 'w') as file:
		for time in tempo_changes_times:
			file.write(str(time) + '\n')
