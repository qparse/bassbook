"""
Test :
python3 wlearn.py
"""

import sys
import logging
sys.path.insert(0, "../../build")
from pyqparse import *

def test_empty_env():
    env = LearningEnv()
    print("Has segment must be false: ", env.has_segment())
    print("Empty Input must be true: ", env.empty_input())
    print("Has WTA must be false: ", env.has_WTA())


def show_count(env):
    dim = env.dimension()
    print("Number of transitions: ", dim)
    # count(i) returns the number of times transition i has been used
    for i in range(0, dim):
        print(f'count of transition {i:2} = {env.count(i)}')
        

def test_training(wta_filepath, weight_code):
    # create an empty scoring env
    env = LearningEnv()
    env.set_debug(True)
    env.set_order(True)
    env.set_weight(Tropical)

    # open WTA (tree grammar) from file
    status = env.import_WTA(wta_filepath, weight_code)
    print("Env has grammar: ", env.has_WTA())

    N = MTUIKind.Note
    C = MTUIKind.Cont
    R = MTUIKind.Rest
    U = MTUISubKind.Undef
    # add a note event of index 0 and duration 1/2 measure
    env.feedr(0, 1, 2, N, U)
    # add a grace note event (duration 0) of index 1
    env.feedr(1, 0, 1, N, U)
    # add two note events of indices 2 and 3, and duration 1/4 measure each
    env.feedr(2, 1, 4, N, U)
    env.feedr(3, 1, 4, N, U)
    # as soon as one full measure has been added (cumulated duration 1)
    # it is processed with the grammar
    print("Bars processed: ", env.processed())
    env.feedr(4, 1, 2, N, U)
    # continuation of the previous note (tie or dot) of duration 1/4
    env.feedr(5, 1, 4, C, U)
    env.feedr(6, 1, 2, N, U)
    # the measure is overful, it is continued in the next measure
    # i.e. the next measure starts with a continuation of 1/4
    env.feedr(5, 1, 4, N, U)
    env.feedr(6, 1, 2, N, U)
    # now the 3d measure is full
    print("Bars processed: ", env.processed())
    return env


def main():
    wta_filepath = "../schema/schema-02.wta"
    weight_code = Tropical
    env = test_training(wta_filepath, weight_code)
    show_count(env)


if __name__ == "__main__":
    main()
